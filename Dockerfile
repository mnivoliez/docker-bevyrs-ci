FROM rust:latest

RUN apt update

RUN apt install -y clang lld

RUN apt install -y libasound2-dev libudev-dev
